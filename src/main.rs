
use std::fmt;

fn main() {
    let mut d = Document::new();

    // ints and strings get `Draw` automatically because they already have implementations for
    // `Display` and `Clone` -- see below
    d.add(2);
    d.add(String::from("hello"));
    d.add(d.clone_boxed());
    d.add(String::from("world"));

    d.draw();
}

struct Document {
    /// `Box<dyn Draw>` means we're storing heap-allocated virtual `Draw` implementations.
    /// The `Box` will be fixed size since it's just a pointer, so it can be stored in a Vec.
    /// The `dyn Draw` part means that we're storing "trait objects" (aka there will be a vtable
    /// involved) rather than devirtualizing as you might if there was only one Draw implementation
    /// in use.
    objects: Vec<Box<dyn Draw>>,
}

impl Document {
    fn new() -> Document {
        return Document { objects: vec![] };
    }

    /// We take a ToDrawBox so that both `Draw` and `Box<dyn Draw>` can be added via the same method
    fn add<B: ToDrawBox>(&mut self, d: B) {
        self.objects.push(d.to_box());
    }
}

/// Toy type to represent the polymorphic behavior
trait Draw {
    /// Draw thyself!
    fn draw(&self);

    /// Can't use plain old Clone because it returns `Self`, which is not allowed for trait objects,
    /// so we make a clone-like method that returns a `Box` instead.
    fn clone_boxed(&self) -> Box<dyn Draw>;
}

/// Document has its own Draw implementation
impl Draw for Document {
    fn draw(&self) {
        println!("<document>");
        self.objects.iter().for_each(|d| d.draw());
        println!("</document>");
    }

    fn clone_boxed(&self) -> Box<dyn Draw> {
        Box::new(Document {
            objects: self.objects.iter().map(|x| x.clone_boxed()).collect(),
        })
    }
}

/// Any implementer of Display and Clone gets Draw for free
impl<T: fmt::Display + Clone + 'static> Draw for T {
    fn draw(&self) {
        println!("{}", self)
    }

    fn clone_boxed(&self) -> Box<dyn Draw> {
        Box::new(self.clone())
    }
}

/// Allow adding either boxed or unboxed things to a Document the same way
trait ToDrawBox {
    fn to_box(self) -> Box<dyn Draw>;
}

impl ToDrawBox for Box<dyn Draw> {
    fn to_box(self) -> Box<dyn Draw> {
        self
    }
}

impl<T: Draw + 'static> ToDrawBox for T {
    fn to_box(self) -> Box<dyn Draw> {
        Box::new(self)
    }
}
